# Web Socket Chat Application: Server



## Overview

This repository is part of an educational project for creating a chat application based on web sockets. The application follows a server-client architecture with a Python server and a React JS client. The user can connect anonymously to the chat application and then start getting messages from the other users. 


The server is an event-driven program which has a main loop which will wait for events from clients. This server will start an instance of Socketio to wait for HTTP requests coming from the client. It will do so by registering namespace classes. These classes come in the form of:


```python
class ExampleNameSpace(socketio.AsyncNamespace):

    def __init__(self):
        super(ExampleNameSpace, self).__init__(NAMESPACE_ROUTE)
        pass
        
    def on_connect(self, sid, environ):
        # Handle the connection of the client with session id "sid"
        pass
        
    def on_disconnect(self, sid):
        # Handle the disconnection of the client with session id "sid"
        pass
        
    async def on_event(self,sid):
        # Handle event
        pass 
```

For our purposes, we need two namespaces: 

+ **ChatNameSpace** which would handle the chat packets and the control packets.

+ **AdministrationNameSpace** which would handle the management operations such as the disconnection of client and the listing of the connected clients.

---

For the control of the application, we decided to use the main controller which would have access to handlers. This main controller would act as a switch and trigger a handler depending on which action was required. Handlers in our case are two instances :

- *ControlSocketHandler*: handles the connections and disconnections by managing a list of users and keeping it up to date. It also creates a room or shared chat where all participants can send a message and it will be received by the others.
- *MessageHandler*: handles the reception of messages and forwarding them and acknowledging that the message was properly shared.

For the management, the server would have an instance of a socket client and would communicate with a specific event. It would then use a pre-shared *key* to reduce the access to sensitive action, in our case disconnecting a client.


## Project Structure

The project is structured as follows:

```
├── config # Configuration files
├── controller # Controller objects and handlers
├── log # Log files
├── main.py # Entry point of the application
├── model # Model objects
├── requirements.txt # Libraries used in the application
├── run # Run functions and daemons
├── static # Static objects shared by the Web Server
├── utils # Utilities and static external libraries
└── venv # Virtual Environment for non-production version
```

## Dependencies 

To accomplish this project we decided to use the following open source libraries:

- [Socketio.js](https://socket.io/)
- [ReactJS](https://reactjs.org/)
- [MobX](https://mobx.js.org/)
- [Sockeio-python](https://python-socketio.readthedocs.io/en/latest/)
- [Click](https://click.palletsprojects.com/en/7.x/)
- [Aiottp](https://aiohttp.readthedocs.io/en/stable/)
- [Eventlet](http://eventlet.net/)
- [Python-deamon](https://travis-ci.org/serverdensity/python-daemon)

## Run Instruction

For starting the Server in development mode the following steps are followed:

Checking if Python 3.6 or higher is available and testing the pip package:

```bash
python3 -V
```

If it's lower, use the following command on Ubuntu:

```bash
sudo apt-get update
sudo apt-get install python3.6
```

Prepare the environment and install the dependencies:

```bash
cd wsca-server/
virtualenv venv
source venv/bin/activate
pip install -r requirements.txt 
```
In case of a problem with pip use the following command:

```bash
sudo apt-get install python3-pip
```

The following instruction assumes the virtual environment is active:
```bash
source venv/bin/activate
```

Start the server:

```bash
python main.py --start
```

Start the server in daemon mode:
```bash
python main.py --start -d
```

Stop the server:
```bash
python main.py --stop
```

List connected clients:
```bash
python main.py -c
```

Disconnect Client "test":
```bash
python main.py -k test
```

_Notes:_

- Logs of the server can be found in **log/chat.log**
- Do not use the key in this repository