import os

import click
import socketio
from aiohttp import web
from socketIO_client import SocketIO

from config.config import LOCALHOST, DEFAULT_PORT, SERVER_RESPONSE_TIME, MANAGEMENT_KEY, DEFAULT_LOG_FILE
from controller.controller import MainController
from run.daemon import AppDaemon
from controller.client_namespace import ManagementNameSpace


@click.command()
@click.option("--start", is_flag=True, help="Start the Server")
@click.option("--stop", is_flag=True, help="Stop the Server")
@click.option("--status", is_flag=True, help="Show the Status of the Server")
@click.option("--daemon", '-d', is_flag=True, help="Run as a Daemon")
@click.option("--logfile", '-f', multiple=False, default="log/chat.log", help="File for the logs", type=str)
@click.option("--kill", '-k', multiple=False, help="Disconnect a client by username", type=str)
@click.option("--connected", '-c', is_flag=True, help="Show connected clients", type=str)
def main(start, stop, status, daemon, logfile, kill, connected):
    sio = socketio.AsyncServer(async_mode='aiohttp', engineio_logger=daemon)
    app = web.Application()
    controller = MainController(sio)

    if logfile and daemon:
        if not os.path.isfile(logfile):
            print("Log file does not exist \nAttempting to create it...")
            try:
                with open(logfile, "w+") as f:
                    f.write("========================== Log File ==========================\n")
            except OSError as e:
                print("An error occurred while creating the logfile \nExiting the program")
                return

            if not os.path.isfile(logfile):
                print("Logfile could not be created \nExiting the program")
                return

    if status:
        runner = AppDaemon(sio, app, controller, logfile)
        runner.is_running()
        return

    if stop:
        runner = AppDaemon(sio, app, controller, logfile)
        runner.stop()
        return

    elif start:

        runner = AppDaemon(sio, app, controller, (logfile if daemon else DEFAULT_LOG_FILE))

        if not runner.is_running():

            if daemon:
                runner.start()

            else:
                runner.run()

    elif kill:
        runner = AppDaemon(sio, app, controller, logfile)

        if runner.is_running():
            sio = SocketIO(LOCALHOST, DEFAULT_PORT)
            admin_namespace = sio.define(ManagementNameSpace, '/admin')
            admin_namespace.emit('kill', {"key": MANAGEMENT_KEY, "username": kill})
            sio.wait(SERVER_RESPONSE_TIME)
            print("Server not responding...")

    elif connected:
        runner = AppDaemon(sio, app, controller, logfile)

        if runner.is_running():
            sio = SocketIO(LOCALHOST, DEFAULT_PORT)
            admin_namespace = sio.define(ManagementNameSpace, '/admin')
            admin_namespace.emit('connected')
            sio.wait(SERVER_RESPONSE_TIME)
            print("Server not responding...")


if __name__ == '__main__':
    main()
