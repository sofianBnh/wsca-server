import sys

from socketIO_client import BaseNamespace


def print_dict(dict_obj):
    print("\n" + "-" * 50)

    for key, val in dict_obj.items():
        print('{} : {}'.format(val, key))
    print("-" * 50 + "\n")


class ManagementNameSpace(BaseNamespace):

    def on_connected(self, connections):
        if connections:
            print_dict(connections)
        else:
            print("No user connected")
        sys.exit(0)

    def on_kill(self, state):
        if state:
            print(state)
            sys.exit(0)
