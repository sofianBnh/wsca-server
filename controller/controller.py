import logging

from config.config import *
from controller.handlers import ControlSocketHandler, SharedSocketHandler
from model.packet import ControlPacket, MessagePacket
from model.user import User


class MainController(object):

    def __init__(self, sio):
        self.control = ControlSocketHandler(sio)
        self.shared = SharedSocketHandler(sio)

    def connection(self, user: User):
        logging.info("Connecting " + user.sid)
        self.control.add_anonymous(user)

    def disconnection(self, sid):
        logging.info("Disconnecting " + sid)
        self.control.remove_user(sid)

    def get_connections(self) -> dict:
        return self.control.users

    async def kill(self, username):
        await self.control.kill(username)

    async def handle_control(self, packet: ControlPacket):
        if packet.action not in SERVER_SUPPORTED_CONTROL_ACTIONS:
            await self.handle_error(packet.sid, "Control Action Not Supported")

        if packet.action == REGISTER:
            await self.control.register(packet.sid, packet.options)

    async def handle_message(self, packet: MessagePacket):

        if packet.action not in MESSAGE_ACTIONS:
            await self.handle_error(packet.sid, "Message Type Not Supported")

        if packet.action == SEND_SHARED:
            sender_username = self.control.get_username(packet.sid)

            if sender_username:
                await self.shared.send_broadcast(packet.sid, sender_username, packet.message)
            else:
                await self.handle_error(packet.sid, "Not registered")

    async def handle_error(self, sid, message: str, event="reject"):
        await self.control.reject(sid, message, event=event)
