import socketio
from config.config import CHAT_NAMESPACE, ADMIN_NAMESPACE, MANAGEMENT_KEY
from controller.controller import MainController
from model.parser import user_builder, packet_builder


class ChatNameSpace(socketio.AsyncNamespace):

    def __init__(self, controller: MainController):
        super(ChatNameSpace, self).__init__(CHAT_NAMESPACE)
        self.controller = controller

    def on_connect(self, sid, environ):
        anonymous_user = user_builder(sid, environ)

        if anonymous_user:
            self.controller.connection(anonymous_user)
        else:
            self.controller.handle_error(sid, "An Error occurred")

    def on_disconnect(self, sid):
        self.controller.disconnection(sid)

    async def on_packet(self, sid, raw_json):
        packet = packet_builder(raw_json, sid)

        if packet:
            await self.controller.handle_message(packet)
        else:
            await self.controller.handle_error(sid, "Malformed Packet")

    async def on_control(self, sid, raw_json):
        packet = packet_builder(raw_json, sid)

        if packet:
            await self.controller.handle_control(packet)
        else:
            await self.controller.handle_error(sid, "Malformed Packet")


class AdministrationNameSpace(socketio.AsyncNamespace):

    def __init__(self, controller: MainController):
        super(AdministrationNameSpace, self).__init__(ADMIN_NAMESPACE)
        self.controller = controller

    async def on_kill(self, sid, commands: dict):
        if not commands or "key" not in commands.keys():
            await self.emit(namespace=ADMIN_NAMESPACE, event="kill",
                            data="Something Happened... Maybe?",
                            room=sid)

        if commands["key"] == MANAGEMENT_KEY:
            if "username" not in commands.keys():
                await self.emit(namespace=ADMIN_NAMESPACE, event="kill", data="Specify username", room=sid)
            else:
                await self.controller.kill(commands['username'])
                await self.emit(namespace=ADMIN_NAMESPACE, event="kill",
                                data="Disconnecting " + commands['username'] + " ...",
                                room=sid)

    async def on_connected(self, sid):
        users = self.controller.get_connections()
        await self.emit(namespace=ADMIN_NAMESPACE, event="connected", data=users, room=sid)
