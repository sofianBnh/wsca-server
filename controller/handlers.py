from abc import ABC

from config.config import SHARED_ROOM, CHAT_NAMESPACE
from model.packet import ControlPacket, MessagePacket
from model.user import User


#####################################################
# /// Base Handler
####

class SocketHandler(ABC):

    def __init__(self, sio):
        self.sio = sio


#####################################################
# /// Message Handlers
####

class SharedSocketHandler(SocketHandler):

    async def send_broadcast(self, sid, username: str, message: str):
        packet = MessagePacket(None, username, message)

        await self.sio.emit(event="sent",
                            data=packet.to_json(),
                            namespace=CHAT_NAMESPACE,
                            room=sid)

        await self.sio.emit(event="packet",
                            data=packet.to_json(),
                            room=SHARED_ROOM,
                            skip_sid=sid,
                            namespace=CHAT_NAMESPACE)

    def join_shared_chat(self, sid):
        self.sio.enter_room(sid=sid,
                            room=SHARED_ROOM,
                            namespace=CHAT_NAMESPACE)


#####################################################
# /// Control Handlers
####


class ControlSocketHandler(SocketHandler):

    def __init__(self, sio):
        super(ControlSocketHandler, self).__init__(sio)
        self.users = {}

    def add_anonymous(self, user: User):
        if user.sid in self.users.keys():
            return
        self.users[user.sid] = "Anonymous"

    async def register(self, sid, username):

        def _is_username_available(user_db) -> bool:
            names = set()
            [(names.add(user_db[x]) if user_db[x] is not None else None) for x in user_db.keys()]
            return username not in names

        def _is_not_registered(user_db):
            return sid in user_db.keys() and user_db[sid] == "Anonymous"

        if _is_not_registered(self.users):

            if not username or username == "":
                await self.reject(sid, message="Empty Username", event="register_error")
                return

            if _is_username_available(self.users):
                self.users[sid] = username
                self.sio.enter_room(sid, SHARED_ROOM, namespace=CHAT_NAMESPACE)
                await self.sio.emit(room=sid, namespace=CHAT_NAMESPACE, event="accept")

            else:
                await self.reject(sid, message="Username Taken", event="register_error")
        else:
            await self.reject(sid, message="An error occurred", event="register_error")

    async def reject(self, sid, message, event="reject"):  # error message
        packet = ControlPacket(None, options=message)
        await self.sio.emit(room=sid, event=event, data=packet.to_json(), namespace=CHAT_NAMESPACE)

    def remove_user(self, sid):
        if sid in self.users.keys():
            del self.users[sid]

    def get_username(self, sid) -> str or None:
        if not self.users[sid]:
            return None
        return self.users[sid]

    def get_sid(self, username) -> str or None:
        for sid, user in self.users.items():
            if user == username:
                return sid
        return None

    async def kill(self, username):

        sid = self.get_sid(username)

        if not sid:
            return

        self.remove_user(sid)
        await self.sio.disconnect(sid=sid, namespace=CHAT_NAMESPACE, )
