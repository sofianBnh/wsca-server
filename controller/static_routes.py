from aiohttp import web


async def index(request):
    """Serve the client-side application."""
    with open('static/root/index.html') as f:
        return web.Response(text=f.read(), content_type='text/html')