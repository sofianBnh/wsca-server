import logging

from aiohttp import web

from config.config import DEFAULT_PORT, DEFAULT_LOG_FILE, DEFAULT_PID_FILE
from controller.server_namespaces import ChatNameSpace, AdministrationNameSpace
from controller.static_routes import index
from utils.daemon import daemon


class AppDaemon(daemon.Daemon):

    def __init__(self, sio, app, controller, logfile=DEFAULT_LOG_FILE, port=DEFAULT_PORT):
        super().__init__(DEFAULT_PID_FILE)
        self.sio = sio
        self.app = app
        self.controller = controller
        self.logfile = logfile
        self.port = port

    def run(self):
        logging.basicConfig(level=logging.DEBUG, filename=self.logfile)

        self.sio.register_namespace(ChatNameSpace(self.controller))
        self.sio.register_namespace(AdministrationNameSpace(self.controller))
        self.sio.attach(self.app)

        self.app.router.add_static('/static', 'static')
        self.app.router.add_get('/', index)
        self.app.router.add_static('/', 'static/root')
        web.run_app(self.app, port=self.port, access_log=logging)
