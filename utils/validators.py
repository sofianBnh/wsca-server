def in_dict(attribute: str, dictionary: dict, domain: list = None) -> bool:
    if not attribute or not dictionary:
        return False

    if attribute not in dictionary.keys():
        return False

    if domain:
        return dictionary[attribute] in domain

    return True

