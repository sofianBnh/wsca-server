from config.config import CONTROL_PACKET, SEND_SHARED, MESSAGE_ACTIONS, CONTROL_ACTIONS, MESSAGE_PACKET, PACKET_TYPES
from model.packet import Packet, MessagePacket, ControlPacket
from model.user import User
from utils.validators import in_dict


def packet_builder(raw_json: dict, sid, ) -> Packet or None:
    if not in_dict("type", raw_json, domain=PACKET_TYPES):
        return None

    if raw_json["type"] == CONTROL_PACKET:
        packet = _control_packet_from_dict(raw_json, sid)
    elif raw_json["type"] == MESSAGE_PACKET:
        packet = _message_packet_from_dict(raw_json, sid)
    else:
        return None

    return packet


def _control_packet_from_dict(json_object: dict, sid) -> ControlPacket or None:
    control_packet = ControlPacket(sid=sid)

    if not in_dict("action", json_object, CONTROL_ACTIONS):
        del control_packet
        return None

    control_packet.action = json_object["action"]
    control_packet.options = json_object["options"]

    del json_object
    return control_packet


def _message_packet_from_dict(json_object: dict, sid) -> MessagePacket or None:
    message_packet = MessagePacket(sid)

    if not in_dict("action", json_object, MESSAGE_ACTIONS):
        del message_packet
        return None

    message_packet.action = json_object["action"]

    if in_dict("receiver", json_object):
        message_packet.remote_user = json_object["receiver"]
    else:
        if not json_object["action"] == SEND_SHARED:
            del message_packet
            return None

    message_packet.message = json_object["message"]

    del json_object
    return message_packet


def user_builder(sid, _) -> User:
    return User(sid)
