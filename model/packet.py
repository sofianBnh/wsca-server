import json
from abc import ABC, abstractmethod

from config.config import MESSAGE_PACKET, SEND_SHARED, CONTROL_PACKET, ERROR


class Packet(ABC):

    def __init__(self, packet_type, action, sid):
        self.type = packet_type
        self.action = action
        self.sid = sid

    @abstractmethod
    def to_json(self) -> str:
        pass


class MessagePacket(Packet):

    def __init__(self, sid, sender_username=None, message=None,
                 action=SEND_SHARED, receiver_username: str = None):
        super(MessagePacket, self).__init__(MESSAGE_PACKET, action, sid)
        if message is None:
            message = {}
        self.receiver_username = receiver_username
        self.sender_username = sender_username
        self.message = message

    def to_json(self) -> str:
        json_dict = {
            "type": MESSAGE_PACKET,
            "action": self.action,
            "sender": self.sender_username,
            "receiver": self.receiver_username,
            "message": self.message
        }

        return json.dumps(json_dict)


class ControlPacket(Packet):

    def __init__(self, sid, action: str = ERROR, options=""):
        super(ControlPacket, self).__init__(CONTROL_PACKET, action, sid)
        self.options = options

    def to_json(self) -> str:
        json_dict = {
            "type": CONTROL_PACKET,
            "action": self.action,
            "options": self.options
        }

        return json.dumps(json_dict)
